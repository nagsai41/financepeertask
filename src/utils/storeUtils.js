import {getAsyncStorageValue, setAsyncStorageValue} from './localStorage';

import {LocalStorageKeys} from '../Constants/localStoreKeys';

export const saveSampleDataToLocalStore = async () => {
  const data = require('../../assets/files/data.json');
  await setAsyncStorageValue(LocalStorageKeys.sampleData, data);
};

export const getSampleDataFromLocalStore = async () => {
  const data = await getAsyncStorageValue(LocalStorageKeys.sampleData);

  return data;
};
