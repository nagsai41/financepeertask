import AsyncStorage from '@react-native-async-storage/async-storage';

export const setAsyncStorageValue = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (e) {
    console.log('Error While saving data:', e);
  }
};

export const getAsyncStorageValue = async (key) => {
  try {
    const jsonValue = await AsyncStorage.getItem(key);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    console.log('Error While saving data:', e);
  }
};
