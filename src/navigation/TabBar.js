import {ApiScreen} from '../screens/ApiScreen';
import {CameraScreen} from '../screens/CameraScreen';
import {HomeScreen} from '../screens/HomeScreen';
import {LocationScreen} from '../screens/LocationScreen';
import React from 'react';
import {StoreScreen} from '../screens/StoreScreen';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

const Tab = createMaterialBottomTabNavigator();

export const TabBar = () => {
  return (
    <Tab.Navigator
      activeColor="#f0edf6"
      inactiveColor="#3e2465"
      barStyle={{backgroundColor: '#694fad'}}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Camera" component={CameraScreen} />
      <Tab.Screen name="Location" component={LocationScreen} />
      <Tab.Screen name="Store" component={StoreScreen} />
      <Tab.Screen name="API" component={ApiScreen} />
    </Tab.Navigator>
  );
};
