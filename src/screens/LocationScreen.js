import {
  Alert,
  PermissionsAndroid,
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';

import {Button} from '../Components/Button';
import Geolocation from '@react-native-community/geolocation';
import {Loader} from '../Components/Loader';
import {Screen} from '../Components/Screen';

export const LocationScreen = () => {
  const [coords, setCoords] = useState();
  const [showLoader, setShowLoader] = useState();

  const getLocation = () => {
    setShowLoader(true);
    Geolocation.getCurrentPosition(
      (position) => {
        setTimeout(() => {
          setCoords(position);
          setShowLoader(false);
        }, 1000);
      },
      (error) => {
        setShowLoader(false);
        Alert.alert('Sorry', 'Error while fetching location: ', error.message);
      },
      {
        enableHighAccuracy: false,
        timeout: 30000,
        maximumAge: 1000,
      },
    );
  };

  useEffect(() => {
    (async () => {
      if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Access Required',
            message: 'This App needs to Access your location',
          },
        );
      }
    })();
  }, []);

  return (
    <Screen>
      <View>
        <Text style={styles.text}>{`Longitudes: ${
          coords?.coords?.latitude || ''
        }`}</Text>
        <Text style={styles.text}>{`Latitudes: ${
          coords?.coords?.longitude || ''
        }`}</Text>
      </View>
      <Text style={styles.infoText}>
        Hit the below button to fetch Current Latitudes and Longitudes
      </Text>
      <Button title="Get Location" onPress={getLocation} />
      {showLoader ? <Loader /> : null}
    </Screen>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 18,
    marginTop: 25,
  },
  infoText: {
    fontSize: 15,
  },
});
