import {Loader} from '../Components/Loader';
import React from 'react';
import {Screen} from '../Components/Screen';
import {Text} from 'react-native';
import {useSampleApi} from '../api/useSampleApi';

export const ApiScreen = () => {
  const data = useSampleApi();

  return (
    <Screen>
      <Text>{data}</Text>
      {data ? null : <Loader />}
    </Screen>
  );
};
