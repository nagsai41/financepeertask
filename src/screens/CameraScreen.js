import {Image, StyleSheet} from 'react-native';
import React, {useState} from 'react';

import {Button} from '../Components/Button';
import {Screen} from '../Components/Screen';
import {launchCamera} from 'react-native-image-picker';

export const CameraScreen = () => {
  const [img, setImg] = useState('');

  const openCamera = async () => {
    const data = await launchCamera();

    setImg(data.assets[0].uri);
  };

  return (
    <Screen>
      <Image source={{uri: img}} style={styles.image} />
      <Button title="Open Camera" onPress={openCamera} />
    </Screen>
  );
};

const styles = StyleSheet.create({
  image: {
    width: 500,
    height: 500,
    resizeMode: 'cover',
  },

  button: {
    // marginTop: 40,
  },
});
