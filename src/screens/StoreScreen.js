import {FlatList, StyleSheet, Text, View} from 'react-native';
import React, {useState} from 'react';
import {
  getSampleDataFromLocalStore,
  saveSampleDataToLocalStore,
} from '../utils/storeUtils';

import {Button} from '../Components/Button';
import {Loader} from '../Components/Loader';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

export const StoreScreen = () => {
  const [data, setData] = useState();
  const [showLoader, setShowLoader] = useState(false);

  const {top} = useSafeAreaInsets();

  const onPressStore = async () => {
    setShowLoader(true);
    await saveSampleDataToLocalStore();

    setTimeout(() => {
      setShowLoader(false);
    }, 1000);
  };

  const onPressFetch = async () => {
    setShowLoader(true);
    const storeData = await getSampleDataFromLocalStore();
    setTimeout(() => {
      setData(storeData);
      setShowLoader(false);
    }, 1000);
  };

  const renderItem = ({item: {userId, title, body} = {}}) => {
    return (
      <View style={styles.cell}>
        <Text style={styles.cellData}>UserId: {userId}</Text>
        <Text style={styles.cellData}>Title: {title}</Text>
        <Text style={styles.cellData}>Body: {body}</Text>
      </View>
    );
  };

  const keyExtractor = ({id}) => id;

  return (
    <>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
        style={{paddingTop: top}}
      />
      <Button
        title="Store data into localStore"
        buttonStyle={styles.button}
        textStyle={styles.buttonText}
        onPress={onPressStore}
      />
      <Button
        title="Fetch data from localStore"
        buttonStyle={styles.button}
        textStyle={styles.buttonText}
        onPress={onPressFetch}
      />
      {showLoader ? <Loader /> : null}
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 15,
  },
  button: {
    marginTop: 10,
  },
  buttonText: {},
  cell: {
    padding: 15,
  },
  cellData: {
    color: 'black',
  },
});
