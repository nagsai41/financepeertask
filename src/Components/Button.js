import {StyleSheet, Text, TouchableOpacity} from 'react-native';

import React from 'react';

export const Button = ({
  title = '',
  onPress = () => {},
  buttonStyle,
  textStyle,
  ...rest
}) => {
  return (
    <TouchableOpacity
      style={{...styles.button, ...buttonStyle}}
      onPress={onPress}
      {...rest}>
      <Text style={{...styles.text, ...textStyle}}>{title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#EF8372',
    padding: 15,
    borderRadius: 10,
    alignSelf: 'center',
    width: '60%',
  },
  text: {
    color: 'white',
    alignSelf: 'center',
    fontSize: 15,
    fontWeight: '500',
  },
});
