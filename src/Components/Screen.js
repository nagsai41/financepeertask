import {ScrollView, StyleSheet} from 'react-native';

import React from 'react';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

export const Screen = ({children, contentContainerStyle, ...rest}) => {
  const {top} = useSafeAreaInsets();
  return (
    <ScrollView
      contentContainerStyle={{
        ...styles.container,
        paddingTop: top,

        ...contentContainerStyle,
      }}
      {...rest}>
      {children}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15,
    justifyContent: 'space-between',
  },
});
