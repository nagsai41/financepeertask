import {ActivityIndicator, Modal, StyleSheet, View} from 'react-native';

import React from 'react';

export const Loader = () => {
  return (
    <Modal visible={true} transparent style={styles.style}>
      <View style={styles.viewStyle}>
        <ActivityIndicator size={'large'} />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  style: {
    flex: 1,
  },
  viewStyle: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
  },
});
