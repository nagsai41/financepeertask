import axios from 'axios';

export const sampleApi = async () => {
  return axios.get('https://catfact.ninja/fact');
};
