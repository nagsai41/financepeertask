import {useEffect, useState} from 'react';

import {sampleApi} from './sampleApiUseCases';

export const useSampleApi = () => {
  const [response, setResponse] = useState('');

  useEffect(() => {
    sampleApi().then(({data}) => {
      setResponse(data?.fact);
    });
  }, []);

  return response;
};
